" An example for a vimrc file.
"
" Maintainer:	Bram Moolenaar <Bram@vim.org>
" Last change:	2002 May 28
"
" To use it, copy it to
"     for Unix and OS/2:  ~/.vimrc
"	      for Amiga:  s:.vimrc
"  for MS-DOS and Win32:  $VIM\_vimrc
"	    for OpenVMS:  sys$login:.vimrc

" When started as "evim", evim.vim will already have done these settings.
if v:progname =~? "evim"
  finish
endif

" Use Vim settings, rather then Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

set autoindent		" always set autoindenting on
if has("vms")
  set nobackup		" do not keep a backup file, use versions instead
else
  set backup		" keep a backup file
endif
set history=50		" keep 50 lines of command line history
set ruler		" show the cursor position all the time
set showcmd		" display incomplete commands
set incsearch		" do incremental searching

" For Win32 GUI: remove 't' flag from 'guioptions': no tearoff menu entries
" let &guioptions = substitute(&guioptions, "t", "", "g")

" Don't use Ex mode, use Q for formatting
map Q gq

" This is an alternative that also works in block mode, but the deleted
" text is lost and it only works for putting the current register.
"vnoremap p "_dp

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
  syntax on
  set hlsearch
endif

" Only do this part when compiled with support for autocommands.
if has("autocmd")

  " Enable file type detection.
  " Use the default filetype settings, so that mail gets 'tw' set to 72,
  " 'cindent' is on in C files, etc.
  " Also load indent files, to automatically do language-dependent indenting.
  filetype plugin indent on

  " For all text files set 'textwidth' to 78 characters.
  " autocmd FileType text setlocal textwidth=78

  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid or when inside an event handler
  " (happens when dropping a file on gvim).
  autocmd BufReadPost *
    \ if line("'\"") > 0 && line("'\"") <= line("$") |
    \   exe "normal g`\"" |
    \ endif

endif " has("autocmd")
" retab
"if has("gui_kde")
	"set guifont=Courier\ 10\ Pitch/16/-1/5/50/0/0/0/1/0
"endif 
if has("gui_running")
        if has("gui_gtk2")
            "set guifont=Terminal/16
	"elseif has("gui_kde")
		"set guifont=Terminal/16
	"elseif has("x11")
            "set guifont=-*-courier-*-*-*-*-18-140-*-*-*-*-*
        "else
            "set guifont=Terminal:h16:cDEFAULT
        endif
endif 

" My Stuff
colorscheme murphy "colorscheme of the chosen ones
set guifont=Menlo:h18
set showmatch "duh
set expandtab
set ts=4
set sw=4
set ai
set scrolloff=5 "suggested by @OvidPerl and I like it
set vb
" set foldmethod=syntax "Syntax-based folding. Reminder: zo to open a fold, zc to close it
au BufNewFile,BufRead *.md set filetype=markdown "auto syntax highlight .md files for Markdown
au BufNewFile,BufRead *.pml set filetype=markdown
au BufNewFile,BufRead *.md setlocal linebreak
au BufNewFile,BufRead *.pml setlocal linebreak

" Plugins to install
" https://github.com/tpope/vim-surround
" Use this for install instructions: https://opensource.com/article/20/2/how-install-vim-plugins
