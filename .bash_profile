PATH=$PATH:/Applications:/usr/local/git/bin/:/Users/brasseur/bin
export PATH
source /Users/brasseur/.aliases
#source /Users/brasseur/perl5/perlbrew/etc/bashrc
#eval $(perl -I$HOME/perl5/lib/perl5 -Mlocal::lib)
source ~/perl5/perlbrew/etc/bashrc

##
# Your previous /Users/brasseur/.bash_profile file was backed up as /Users/brasseur/.bash_profile.macports-saved_2014-01-03_at_17:13:31
##

# MacPorts Installer addition on 2014-01-03_at_17:13:31: adding an appropriate PATH variable for use with MacPorts.
export PATH=/opt/local/bin:/opt/local/sbin:$PATH
# Finished adapting your PATH environment variable for use with MacPorts.

